// JavaScript Document
// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs"),
	path = require("path");
	
	
	
var jsdom = require('jsdom').jsdom;

 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(request, response){
	// This callback runs when a new connection is made to our HTTP server.
	//The following taken from user Andy Zarzycki at http://stackoverflow.com/questions/28822034/simple-node-js-server-that-sends-htmlcss-as-response
	fs.readFile('./' + request.url, function(err, data) {
        if (!err) {
            var dotoffset = request.url.lastIndexOf('.');
            var mimetype = dotoffset == -1
                            ? 'text/plain'
                            : {
                                '.html' : 'text/html',
                                '.ico' : 'image/x-icon',
                                '.jpg' : 'image/jpeg',
                                '.png' : 'image/png',
                                '.gif' : 'image/gif',
                                '.css' : 'text/css',
                                '.js' : 'text/javascript'
                                }[ request.url.substr(dotoffset) ];
            response.setHeader('Content-type' , mimetype);
            response.end(data);
            console.log( request.url, mimetype );
        } else {
            console.log ('file not found: ' + request.url);
            response.writeHead(404, "Not Found");
            response.end();
        }
    });
	
});
app.listen(3456);
 
// Do the Socket.IO magic:
var io = socketio.listen(app);
//Keep track of an array of rooms on the server side. 
var rooms = new Array();
var roomNames = new Array();
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
 	socket.on('user_login',function(data){
		console.log("username: "+data["username"]);
		for(i = 0; i < rooms.length; i++){
			console.log("Room: "+ i + ": " + rooms[i].name);
			io.sockets.emit("push_existing_chatroom",{room:rooms[i].name}) // broadcast the message to other users
		}
		
		
	});
	socket.on('new_chatroom_created',function(data){
		console.log("new chatroom: "+data["room"].name);
		console.log(roomNames.indexOf(String(data["room"].name)));
		if(roomNames.indexOf(data["room"].name) === -1){
			rooms.push(data["room"]);
			roomNames.push(data["room"].name);
			for(i = 0; i < rooms.length; i++){
				console.log("Room "+ i + ": " + rooms[i].name);
			}
			io.sockets.emit("push_new_chatroom",{newRoom:data["room"].name, owner:data["room"].owner }) // broadcast the message to other users
		}else{
			io.sockets.emit("new_chatroom_denied",{owner:data["room"].owner});	
		}
	});
	socket.on('user_joined_chatroom', function(data) {
		
		for(i = 0; i < rooms.length; i++){
			if(rooms[i].name == data["roomName"]){
				console.log("Password entered: " + data["password"]);
				console.log("Room password: " + rooms[i].password);
				if(rooms[i].password == data["password"]){
					console.log("User: " + data["user"] + " joined chatroom " + data["roomName"]);
					var currentUsers = rooms[i].users;
					if(currentUsers.indexOf(data["user"]) === -1){
						rooms[i].users.push(data["user"]);
						rooms[i].messages.push(data["userJoinedMessage"]);
						for(j = 0; j < rooms[i].users.length; j++){
							console.log(rooms[i].name + " users: " + rooms[i].users[j]);
						}
						io.sockets.emit("user_joined_this_chatroom",{roomJoined:rooms[i], userJoined:data["user"], access:rooms[i].access, pw:rooms[i].password});
						break;
					}else{
						//Deny the user
						console.log("It got caught down here.");	
					}
				}else{
					console.log("Password incorrect");
					io.sockets.emit("password_incorrect");
					//emit that the password was wrong	
				}
			}
		}
	});
	socket.on('user_left_chatroom', function(data) {
		console.log("User: " + data["user"] + " left chatroom " + data["roomName"]);
		for(i = 0; i < rooms.length; i++){
			if(rooms[i].name == data["roomName"]){
				var currentUsers = rooms[i].users;
				if(currentUsers.indexOf(data["user"]) > -1){
					var indexOfUserThatLeft = currentUsers.indexOf(data["user"]);
					
					rooms[i].users.splice(indexOfUserThatLeft,1);
					rooms[i].messages.push(data["userLeftMessage"]);
					for(j = 0; j < rooms[i].users.length; j++){
						console.log(rooms[i].name + " users: " + rooms[i].users[j]);
					}
					io.sockets.emit("user_left_this_chatroom",{roomLeft:rooms[i], userLeft:data["user"]})
					break;
				}else{
					//Deny the user
					console.log("It got caught down here.");	
				}
			}
		}
	});
	socket.on('user_kicked_chatroom', function(data) {
		console.log("User: " + data["user"] + " was kicked out of chatroom " + data["roomName"]);
		for(i = 0; i < rooms.length; i++){
			if(rooms[i].name == data["roomName"]){
				var currentUsers = rooms[i].users;
				if(currentUsers.indexOf(data["user"]) > -1){
					var indexOfUserThatLeft = currentUsers.indexOf(data["user"]);
					rooms[i].users.splice(indexOfUserThatLeft,1);
					rooms[i].messages.push(data["userKickedMessage"]);
					for(j = 0; j < rooms[i].users.length; j++){
						console.log(rooms[i].name + " users: " + rooms[i].users[j]);
					}
					io.sockets.emit("user_kicked_from_chatroom",{roomLeft:rooms[i], userLeft:data["user"]})
					break;
				}else{
					//Deny the user
					console.log("It got caught down here.");	
				}
			}
		}
	});
	socket.on('user_banned_chatroom', function(data) {
		console.log("User: " + data["user"] + " was banned from chatroom " + data["roomName"]);
		for(i = 0; i < rooms.length; i++){
			if(rooms[i].name == data["roomName"]){
				var currentUsers = rooms[i].users;
				if(currentUsers.indexOf(data["user"]) > -1){
					var indexOfUserThatLeft = currentUsers.indexOf(data["user"]);
					rooms[i].users.splice(indexOfUserThatLeft,1);
					rooms[i].messages.push(data["userBannedMessage"]);
					rooms[i].banned.push(data["user"]);
					for(j = 0; j < rooms[i].users.length; j++){
						console.log(rooms[i].name + " users: " + rooms[i].users[j]);
					}
					io.sockets.emit("user_banned_from_chatroom",{roomLeft:rooms[i], userLeft:data["user"]})
					break;
				}else{
					//Deny the user
					console.log("It got caught down here.");	
				}
			}
		}
	});
	socket.on('deleted_chatroom', function(data) {
		console.log("User: " + data["user"] + " attempted to delete " + data["roomName"]);
		for(i = 0; i < rooms.length; i++){
			if(rooms[i].name == data["roomName"]){
				if(rooms[i].owner == data["user"]){
					var currentRoom = rooms[i].name;
					var currentRoomObject = rooms[i];
					if(currentRoom.indexOf(data["roomName"]) > -1){
						var indexOfDeletedRoom = currentRoom.indexOf(data["roomName"]);
						rooms.splice(indexOfDeletedRoom,1);
						roomNames.splice(indexOfDeletedRoom,1);
						console.log(currentRoom);
						io.sockets.emit("chatroom_deleted",{deletedRoom:currentRoomObject,roomNames:roomNames})
						break;
					}else{
						//Deny the user
						console.log("It got caught down here.");	
					}
				}else{
					console.log("A non-owner attempted to delete.")
					io.sockets.emit("delete_failed");	
				}
			}
		}
	});
	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 		//Get the room object from the rooms array
		console.log("message: "+data["message"]+" sent to room "+data["currentRoom"]); // log it to the Node.JS output
		for(i = 0; i < rooms.length; i++){
			if(rooms[i].name == data["currentRoom"]){
				rooms[i].messages.push(data["newmsg"]);
				var room = rooms[i];
				break;
			}
		}
		io.sockets.emit("message_to_client",{message:data["message"], room:room, sender:data["sender"]}) // broadcast the message to other users
	});
	socket.on('private_message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 		//Get the room object from the rooms array
		console.log("message: "+data["message"]+" sent to room "+data["currentRoom"]+" privately for " + data["sentTo"]); // log it to the Node.JS output
		for(i = 0; i < rooms.length; i++){
			if(rooms[i].name == data["currentRoom"]){
				rooms[i].messages.push(data["newmsg"]);
				var room = rooms[i];
				break;
			}
		}
		io.sockets.emit("private_message_to_client",{message:data["message"], room:room, sender:data["sender"], sendTo:data["sendTo"]}) // broadcast the message to other users
	});
});