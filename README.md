# Creative Portion - Module 6
Brody Roush, ID 431439
Ben Rosenkranz, ID 429269
Site Link: http://ec2-54-191-90-165.us-west-2.compute.amazonaws.com:3456/client.html
Enter your username to begin chatting! 
1. The first is implementing the ability to maintain a running log of a chat in the chat room, so even if you leave and return, or enter mid-conversation, you can see the comments that were made prior. 
2. The second part of our creative was giving owners the ability to delete chatrooms that they have created.

